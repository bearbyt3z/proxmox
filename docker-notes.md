# Docker configuration notes

## Using docker without `sudo`

Create the docker group (if not exists):
```bash
sudo groupadd docker
```

Add your user to the docker group:
```bash
sudo usermod -aG docker $USER
```

Source: [Docker Docs: Post-installation steps for Linux](https://docs.docker.com/engine/install/linux-postinstall/)
