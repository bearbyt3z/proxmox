# Proxmox configuration notes

## Disk initialization error when installing Proxmox

Just reduce the partition size a little (0.1 GB should be enough).

Source: [Unable to initialize physical volume /dev/sda3](https://forum.proxmox.com/threads/unable-to-initialize-physical-volume-dev-sda3.62570/)

## Remove "no valid subscription" alert when logging into Proxmox

On the Proxmox console:

```bash
cd /usr/share/javascript/proxmox-widget-toolkit/  
cp proxmoxlib.js proxmoxlib.js.backup
```

Change in the `proxmoxlib.js` file:

```javascript
Ext.Msg.show({
    title: gettext('No valid subscription'),
    icon: Ext.Msg.WARNING,
    message: Proxmox.Utils.getNoSubKeyHtml(res.data.url),
    buttons: Ext.Msg.OK,
    callback: function(btn) {
        if (btn !== 'ok') {
            return;
        }
        orig_cmd();
    },
});
```

to:

```javascript
void({
    title: gettext('No valid subscription'),
    icon: Ext.Msg.WARNING,
    message: Proxmox.Utils.getNoSubKeyHtml(res.data.url),
    buttons: Ext.Msg.OK,
    callback: function(btn) {
        if (btn !== 'ok') {
            return;
        }
        orig_cmd();
    },
});
```

Run the command:

```bash
service pveproxy reload
```

Remove cache in your web browser (e.g.: CTRL + F5).

## Adding `pve-no-subscription` repository and disabling `pve-enterprise`

On the left: Datacenter -> Proxmox node  
On the right: Updates -> Repositories -> Add -> No-Subscription

Click on the `pve-enterprise` -> Disable

### Using console

Add following to the `/etc/apt/sources.list` file:

```bash
deb http://download.proxmox.com/debian bullseye pve-no-subscription
```

Comment following line in the `/etc/apt/sources.list.d/pve-enterprise.list` file:

```bash
# deb https://enterprise.proxmox.com/debian bullseye pve-enterprise
```

## Templates update

Location: Datacenter -> Proxmox node -> local -> CT Templates

Using the Proxmox console:

```bash
pveam update
```

## Dark theme

No official dark theme at the moment...

GitHub repository: [Weilbyte/PVEDiscordDark](https://github.com/Weilbyte/PVEDiscordDark)

YouTube presentation: [Proxmox Dark Theme - It's FINALLY Here!](https://www.youtube.com/watch?v=2qxZvP7K2To)

Forum post: [Official DarkMode](https://forum.proxmox.com/threads/official-darkmode.74609)

## Change primary Proxmox VE IP address

Change IP address in these files:

```bash
/etc/network/interfaces
/etc/hosts
```
