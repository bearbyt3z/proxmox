# Manjaro server configuration notes

## Enable SSH service

Install OpenSSH:
```bash
sudo pacman -S openssh
```

Check status (should be disabled by default):
```bash
sudo systemctl status sshd.service
```

Enable and launch SSH Service:
```bash
sudo systemctl enable sshd.service
sudo systemctl start sshd.service
```

If needed edit SSH daemon config file:
```bash
sudo vi /etc/ssh/sshd_config
```

Source: [tuxfixer.com: How to Enable SSH Service in Manjaro Linux](https://tuxfixer.com/configure-ssh-service-in-manjaro-linux/)

## Setup xrdp (for use with Remmina)

### Enable AUR support

Select "Add/Remove Software" -> Preferences -> AUR -> Enable AUR support

### Install xrdp and xorgxrdp

First install the `check` package:

```bash
sudo pacman -S check
```

Then install `xrdp` and `xorgxrdp` from AUR using the "Add/Remove Software" app (accepting PGP signature may be required).

### Configure xrdp

Add the following line to the `/etc/X11/Xwrapper.config` file:
```bash
allowed_users=anybody
```
If the file doesn't exist, create it.

In the `~/.xinitrc` file change the following line:
```bash
local dbus_args=(--sh-syntax --exit-with-session)
```

to:
```bash
local dbus_args=(--sh-syntax)
```

For example:
```bash
get_session(){
    #local dbus_args=(--sh-syntax --exit-with-session)
    local dbus_args=(--sh-syntax)
    case $1 in
```

These steps should be enough, but there are some other modifications in the references links.

### References

* General `xrdp` installation: [How to Setup xrdp in Manjaro Linux](https://rajasekaranp.medium.com/how-to-setup-xrdp-in-manjaro-linux-e176b22bd347)

* `~/.xinitrc` modifications: [XRDP on Manjaro (fixing the blank screen issue)](https://www.adamlabay.net/2021/08/28/xrdp-on-manjaro-fixing-the-blank-screen-issue/)

